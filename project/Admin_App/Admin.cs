﻿using Admin_App.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Admin_App
{
    public partial class Admin_AutoChair : Form
    {
        public Admin_AutoChair()
        {
            InitializeComponent();
        }

        private void btn_Choice_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            tb_Image.Text = openFileDialog1.FileName;
        }
        private bool CheckChair()
        {
            if (tb_Name.Text.Length < 1 
                || tb_Descri.Text.Length < 1 
                || tb_Price.Text.Length < 1
                || tb_Image.Text.Length < 1
                )
            {
                MessageBox.Show("There is empty fild", "Error");
                return false;
            }

            return true;

        }
        private void btn_Add_Click(object sender, EventArgs e)
        {
            if (CheckChair())
            {
                Sender sender1 = new Sender();
                Chair chair = new Chair();
                chair.Name = tb_Name.Text;
                chair.Discription = tb_Descri.Text;
                chair.Prise = tb_Price.Text;
                byte[] imgdata = System.IO.File.ReadAllBytes(tb_Image.Text);
                chair.Image = imgdata;
                sender1.SendCommand("[ADDCHAIR]");
                Thread.Sleep(200);
                sender1.SendSizeOfObject(sender1.GetSizeOfObject<Chair>(chair));
                Thread.Sleep(200);
                sender1.SendChair(chair);
                sender1.Close();
                MessageBox.Show("Added", "Success");
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        Point lastPoint;
        private void Admin_AutoChair_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void Admin_AutoChair_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }
    }
}
